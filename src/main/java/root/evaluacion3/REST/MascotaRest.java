/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluacion3.REST;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evaluacion3.dao.MascotasJpaController;
import root.evaluacion3.dao.exceptions.NonexistentEntityException;
import root.evaluacion3.entity.Mascotas;

/**
 *
 * @author surro
 */
@Path("mascota")
public class MascotaRest {
  
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response list(){
    MascotasJpaController dao = new MascotasJpaController();
    
    List<Mascotas> list = dao.findMascotasEntities();
    return Response.ok(200).entity(list).build();
  }
  
  @POST
  @Produces(MediaType.APPLICATION_JSON)     
  public Response crear(Mascotas mascota){
    try {
      MascotasJpaController dao = new MascotasJpaController();
      
      dao.create(mascota);
    } catch (Exception ex) {
      Logger.getLogger(MascotaRest.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok(200).entity(mascota).build();
  }
  
  @PUT
  @Produces(MediaType.APPLICATION_JSON)     
  public Response modificar(Mascotas mascota){
    try {
      MascotasJpaController dao = new MascotasJpaController();
      
      dao.edit(mascota);
    } catch (Exception ex) {
      Logger.getLogger(MascotaRest.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok(200).entity(mascota).build();
  }
  
  @DELETE
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/{id}")
  public Response Eliminar(@PathParam("id") String id){
    try {
      MascotasJpaController dao = new MascotasJpaController();
      
      dao.destroy(id);
    } catch (NonexistentEntityException ex) {
      Logger.getLogger(MascotaRest.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok("mascota eliminada").build();
  }
}
